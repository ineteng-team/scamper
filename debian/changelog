scamper (20211212-1) unstable; urgency=medium

  * Update watch file with new location
  * New upstream version 20211212
  * Add Rules-Requires-Root: no to debian/control
  * Bump debhelper-compat version to 13
  * Remove manpages for binaries not shipped
  * Remove irl from uploaders (Closes: #970373)

 -- Ana Custura <ana@netstat.org.uk>  Thu, 20 Jan 2022 13:45:28 +0000

scamper (20191102-1) unstable; urgency=medium

  * New upstream version 20191102 (Closes: #935276)
  * Bumps DH version, removes legacy compat file
  * Update S-V to 4.4.1 (no changes needed)

 -- Ana Custura <ana@netstat.org.uk>  Thu, 02 Jan 2020 20:59:32 +0000

scamper (20181219-1) unstable; urgency=medium

  * New upstream version 20181219
  * Updates Standards-Version to 4.3.0

 -- Ana Custura <ana@netstat.org.uk>  Sat, 05 Jan 2019 16:38:31 +0000

scamper (20180504-2) unstable; urgency=medium

  * Updates VCS fields
  * Removes unnecessary dependency on dh-autoreconf
  * Updates Standards-Version to 4.2.0

 -- Ana Custura <ana@netstat.org.uk>  Sun, 12 Aug 2018 17:13:52 +0100

scamper (20180504-1) unstable; urgency=medium

  * New upstream version 20180504

 -- Ana Custura <ana@netstat.org.uk>  Sun, 27 May 2018 23:06:25 +0100

scamper (20180309-1) unstable; urgency=medium

  * New upstream version 20180309

 -- Ana Custura <ana@netstat.org.uk>  Tue, 27 Mar 2018 13:16:35 +0100

scamper (20171204-2) unstable; urgency=medium

  * Various updates and fixes
  * debian/control:
    - Updates project homepage link to https
    - Updates to debhelper compat version 11
    - Replaces outdated 'priority-extra' to 'priority-optional'
  * debian/changelog
    - Removes trailing whitespace
    - Bumps version number
  * debian/compat
    - Updates to version 11
  * debian/watch
    - Updates watch link to https
 -- Ana Custura <ana@netstat.org.uk>  Fri, 26 Jan 2018 11:39:44 +0000

scamper (20171204-1) unstable; urgency=medium

  * New upstream version 20171204
  * debian/control:
   - Update Standards-Version to 4.3.0

 -- Ana Custura <ana@netstat.org.uk>  Tue, 16 Jan 2018 01:06:05 +0000

scamper (20170822-1) unstable; urgency=medium

  * New upstream version 20170822
  * Adds Ana Custura to uploaders
  * debian/control:
   - Update Standards-Version to 4.1.0

 -- Ana Custura <ana@netstat.org.uk>  Sat, 02 Sep 2017 13:59:08 +0100

scamper (20161204a-1) unstable; urgency=medium

  * New upstream version 20161204a
  * debian/control:
   - Update Standards-Version to 4.0.1

 -- Iain R. Learmonth <irl@debian.org>  Mon, 21 Aug 2017 17:34:04 +0100

scamper (20161204-1) unstable; urgency=medium

  [ Ana C. Custura ]
  * Converted copyright file to DEP-5 format

  [ Iain R. Learmonth ]
  * New upstream version 20161204

 -- Iain R. Learmonth <irl@debian.org>  Tue, 24 Jan 2017 16:42:48 +0000

scamper (20161113-1) unstable; urgency=medium

  * New upstream version 20161113.

 -- Iain R. Learmonth <irl@debian.org>  Sat, 03 Dec 2016 15:17:37 +0000

scamper (20141211f-1) unstable; urgency=medium

  * New upstream version 20141211f.

 -- Iain R. Learmonth <irl@debian.org>  Mon, 19 Sep 2016 15:37:15 +0100

scamper (20141211e-1) unstable; urgency=medium

  * New upstream version 20141211e.
  * debian/control:
   - Bump Standards-Version to 3.9.8.
  * debian/rules:
   - Do not install sc_warts2csv. This does not have a man page and
     appears to only be included for SamKnows.
   - Enable build hardening.

 -- Iain R. Learmonth <irl@debian.org>  Tue, 05 Jul 2016 21:53:47 +0100

scamper (20141211d-1) unstable; urgency=medium

  * New upstream release
  * debian/control:
   - Maintainer is now the Internet Measurement Team (pkg-netmeasure)
   - Added myself to uploaders
   - Added Vcs-* fields for Git repository on Alioth

 -- Iain R. Learmonth <irl@debian.org>  Thu, 18 Feb 2016 16:24:14 +0000

scamper (20141031-1) unstable; urgency=medium

  * New upstream release

 -- Matt Brown <mattb@debian.org>  Fri, 31 Oct 2014 22:43:02 +0000

scamper (20140530-1) unstable; urgency=medium

  * New upstream release
  * Bumped Standards-Version to 3.9.6 (no changes required).

 -- Matt Brown <mattb@debian.org>  Fri, 24 Oct 2014 00:20:12 +0100

scamper (20140122-1) unstable; urgency=medium

  * New upstream release. Closes: #736192.

 -- Matt Brown <mattb@debian.org>  Sun, 26 Jan 2014 15:41:42 +0000

scamper (20130824-1) unstable; urgency=low

  * New upstream release.
  * The following packaging updates were contributed by Zack Weinberg:
    - Update upstream contact and licensing information. Add watchfile.
    - Bump to debhelper compat level 9 and enable multiarch for
      libscamperfile0. (Note that due to lack of packaging manpower, we
      are not providing a symbols file for libscamperfile0 at this time.)
    - Use dh-autoreconf instead of autotools-dev; facilitates ppc64el
      support. Patch by Logan Rosen, adjusted to use upstream's
      bootstrap.pl instead of patching configure.ac. Closes: #733581.
    - Standards-Version: 3.9.5 (no further changes required).
  * Thanks Zack!

 -- Matt Brown <mattb@debian.org>  Sun, 26 Jan 2014 15:17:09 +0000

scamper (20111202b-1) unstable; urgency=low

  * New upstream release
  * Removes libscamperfile dependency from the main scamper binary.

 -- Matt Brown <mattb@debian.org>  Wed, 04 Apr 2012 22:50:19 +0100

scamper (20111202a-1) unstable; urgency=low

  * New upstream release
  * Add missing dependency on autotools-dev.
  * Bump Standards-Version. No changes required.

 -- Matt Brown <mattb@debian.org>  Tue, 20 Mar 2012 23:02:50 +0000

scamper (20110503-1) unstable; urgency=low

  * New upstream release
    - New tools and modules.
    - New library for accessing scamper dump files.
  * Switch to dpkg-source 3.0 (quilt) format
  * Major package re-org to deal with new autotooled scamper source.
    - Use dh(1) to do most of our rules.
    - Bump debhelper compat level to 8.

 -- Matt Brown <mattb@debian.org>  Sun, 08 May 2011 22:27:18 +0100

scamper (20100517-1) unstable; urgency=low

  * New upstream release
  * Fix mistake in long package description (Closes: #589813).

 -- Matt Brown <mattb@debian.org>  Sat, 16 Oct 2010 15:51:34 +0100

scamper (20070523n-1) unstable; urgency=low

  * New upstream release

 -- Matt Brown <mattb@debian.org>  Sat, 26 Jul 2008 16:18:16 +0100

scamper (20070523m-1) unstable; urgency=low

  * New upstream release

 -- Matt Brown <mattb@debian.org>  Sun, 22 Jun 2008 17:39:36 +0100

scamper (20070523i-2) unstable; urgency=low

  * Correct copyright documentation to point at GPL-2 specifically rather than
    the common GPL symlink which currently points to GPLv3.

 -- Matt Brown <mattb@debian.org>  Sat, 22 Dec 2007 01:12:47 +0000

scamper (20070523i-1) unstable; urgency=low

  * Initial release (Closes: #456995)

 -- Matt Brown <mattb@debian.org>  Wed, 19 Dec 2007 22:12:40 +0000

