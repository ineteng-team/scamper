Source: scamper
Section: admin
Priority: optional
Maintainer: Internet Measurement Packaging Team <pkg-netmeasure-discuss@lists.alioth.debian.org>
Uploaders: Matt Brown <mattb@debian.org>, Ana Custura <ana@netstat.org.uk>
Build-Depends: debhelper-compat (= 13), autoconf, automake
Build-Conflicts: autoconf2.13, automake1.4
Standards-Version: 4.4.1
Vcs-Git: https://salsa.debian.org/ineteng-team/scamper.git
Vcs-Browser: https://salsa.debian.org/ineteng-team/scamper
Homepage: https://www.caida.org/tools/measurement/scamper/
Rules-Requires-Root: no

Package: scamper
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: parallel Internet measurement utility
 scamper is a program that is able to conduct Internet measurement
 tasks to large numbers of IPv4 and IPv6 addresses, in parallel, to
 fill a specified packets-per-second rate. Currently, it supports the
 well-known ping and traceroute techniques, as well as MDA traceroute,
 alias resolution, some parts of tbit, sting, and neighbour discovery.
 .
 scamper can do ICMP-based Path MTU discovery. scamper starts with the
 outgoing interface's MTU and discovers the location of Path MTU
 bottlenecks. scamper performs a PMTUD search when an ICMP
 fragmentation required message is not returned to establish the PMTU
 to the next point in the network, followed by a TTL limited search to
 infer where the failure appears to occur.

Package: libscamperfile0
Section: libs
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: file access library for scamper's binary dump format
 scamper is a program that is able to conduct Internet measurement
 tasks to large numbers of IPv4 and IPv6 addresses, in parallel, to
 fill a specified packets-per-second rate. Currently, it supports the
 well-known ping and traceroute techniques, as well as MDA traceroute,
 alias resolution, some parts of tbit, sting, and neighbour discovery.
 .
 This package contains the library that provides access to the binary output
 files that scamper can produce in certain modes.

Package: libscamperfile0-dev
Section: libdevel
Architecture: any
Depends: libscamperfile0 (= ${binary:Version}), ${misc:Depends}
Provides: libscamperfile-dev
Conflicts: libscamperfile-dev
Description: development headers for scamper's binary dump file access library
 scamper is a program that is able to conduct Internet measurement
 tasks to large numbers of IPv4 and IPv6 addresses, in parallel, to
 fill a specified packets-per-second rate. Currently, it supports the
 well-known ping and traceroute techniques, as well as MDA traceroute,
 alias resolution, some parts of tbit, sting, and neighbour discovery.
 .
 This package contains development headers and other ancillary files for the
 libscamperfile library.
